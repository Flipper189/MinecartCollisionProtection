package de.flipper189.MinecartCollisionProtection;

import org.bukkit.plugin.java.JavaPlugin;

public class MinecartCollisionProtection extends JavaPlugin {

	@Override
	public void onEnable() {
		// prepare default configuration
		getConfig().addDefault("protect.minecart_when_is_empty", false);
		getConfig().addDefault("protect.minecart_with_player", true);
		getConfig().addDefault("protect.minecart_with_animal", true);
		getConfig().addDefault("protect.minecart_with_villiger", true);
		getConfig().addDefault("protect.minecart_with_chest", false);
		getConfig().addDefault("protect.minecart_with_hopper", false);
		getConfig().options().copyDefaults(true);
		saveConfig();

		getServer().getPluginManager().registerEvents(new Listeners(this), this);
	}

	@Override
	public void onDisable() {
	}

}
