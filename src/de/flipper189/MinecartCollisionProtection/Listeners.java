// This part based on MinecartCollisions of Bobcat00
// https://github.com/Bobcat00/MinecartCollisions/blob/master/src/main/java/com/bobcat00/minecartcollisions/Listeners.java

package de.flipper189.MinecartCollisionProtection;

import org.bukkit.Location;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Monster;
import org.bukkit.entity.NPC;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.minecart.HopperMinecart;
import org.bukkit.entity.minecart.RideableMinecart;
import org.bukkit.entity.minecart.StorageMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleMoveEvent;

public final class Listeners implements Listener {
	private MinecartCollisionProtection plugin;

	public Listeners(MinecartCollisionProtection plugin) {
		this.plugin = plugin;
	}

	// check the configuration if the minecart type is enabled
	private boolean checkMinecartType(Minecart minecart) {
		if (minecart instanceof RideableMinecart) {
			if (minecart.isEmpty()) {
				return plugin.getConfig().getString("protect.minecart_when_is_empty") == "true";
			} else if (minecart.getPassengers().get(0) instanceof Player) {
				return plugin.getConfig().getString("protect.minecart_with_player") == "true";
			} else if (minecart.getPassengers().get(0) instanceof Animals) {
				return plugin.getConfig().getString("protect.minecart_with_animal") == "true";
			} else if (minecart.getPassengers().get(0) instanceof Villager) {
				return plugin.getConfig().getString("protect.minecart_with_villiger") == "true";
			}

		} else if (minecart instanceof StorageMinecart) {
			return plugin.getConfig().getString("protect.minecart_with_chest") == "true";

		} else if (minecart instanceof HopperMinecart) {
			return plugin.getConfig().getString("protect.minecart_with_hopper") == "true";
		}

		return false;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onVehicleMove(VehicleMoveEvent event) {

		if (!(event.getVehicle() instanceof Minecart))
			return;

		Minecart minecart = (Minecart) event.getVehicle();

		if (checkMinecartType(minecart)) {
			Location minecartLocation = event.getTo();
			double minecartX = minecartLocation.getX();
			double minecartZ = minecartLocation.getZ();

			Location minecartFromLocation = event.getFrom();
			double minecartFromX = minecartFromLocation.getX();
			double minecartFromZ = minecartFromLocation.getZ();

			for (Entity entity : minecart.getNearbyEntities(0.4, 0.4, 0.4)) {
				if (((entity instanceof Monster) || (entity instanceof Animals) || (entity instanceof NPC))) {
					if (!entity.isInsideVehicle()) {
						// Entity is a monster, animal, or NPC and is not riding in a minecart.
						// Move the entity further away from the minecart.
						// This may kill the entity (sucks to be him!).

						Location entityLocation = entity.getLocation();
						double entityX = entityLocation.getX();
						double entityZ = entityLocation.getZ();
						double newX = entityX;
						double newZ = entityZ;

						if ((minecartFromX == minecartX) || (minecartFromZ == minecartZ)) {
							// Minecart moving straight, move entity further away on a diagonal by changing
							// X and Z

							if (entityX < minecartX) {
								newX = entityX - 1.0;
							} else {
								newX = entityX + 1.0;
							}
							if (entityZ < minecartZ) {
								newZ = entityZ - 1.0;
							} else {
								newZ = entityZ + 1.0;
							}

						} else {
							// Minecart moving on a diagonal, so move entity to the side by changing X or Z

							if (Math.abs(minecartX - entityX) > Math.abs(minecartZ - entityZ)) {
								if (entityX < minecartX) {
									newX = entityX - 1.0;
								} else {
									newX = entityX + 1.0;
								}
							} else {
								if (entityZ < minecartZ) {
									newZ = entityZ - 1.0;
								} else {
									newZ = entityZ + 1.0;
								}
							}
						}

						entityLocation.setX(newX);
						entityLocation.setZ(newZ);
						entity.teleport(entityLocation);

					}

				}

			} // end for each entity

		} // end if minecart is carrying a player

	} // end event handler

}
