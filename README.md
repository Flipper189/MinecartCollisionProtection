# MinecartCollisionProtection


## Description
Minecart Collision Protection is a plugin for Minecraft Spigot and Paper Servers.
It prevents that a Minecart can be stopped by an entity.


A part of this plugin is based on MinecartCollisions by Bobcat00
https://github.com/Bobcat00/MinecartCollisions

## Usage
Enable or disable the minecart types in config.yml that should be protected from collisions.


## Installation
Put the .jar file in the plugins folder from your server and restart the server.


## Requirements
Spigot/Paper Server 1.13.2 or higher


## Download
- [Version 1.0.3](https://gitlab.com/Flipper189/minecartcollisionprotection/-/blob/main/build/MinecartCollisionProtection-1.0.3.jar)
- [Version 1.0.2](https://gitlab.com/Flipper189/minecartcollisionprotection/-/blob/main/build/MinecartCollisionProtection-1.0.2.jar)


- [Change Log](https://gitlab.com/Flipper189/MinecartCollisionProtection/-/blob/main/CHANGELOG)


## License
[GNU GPLv3](https://gitlab.com/Flipper189/minecartcollisionprotection/-/blob/main/LICENSE)